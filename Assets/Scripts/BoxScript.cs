﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class BoxScript : MonoBehaviour, IInteractable {

    /// <summary>
    /// This script will move a box 1 space up. Once moved cannot be moved again.
    /// </summary>

    public bool hasMoved = false;
    public float moveDistance;
    public Vector3 moveDirection;

    public void Action()
    {
        if(hasMoved == false)
        {
            transform.Translate(moveDirection * moveDistance * Time.deltaTime);
            hasMoved = true;
        }
        
    }

    public void ResetInteractable()
    {
        return;
    }
}
