﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

    public GameObject targetObject;
    private Vector3 offset;

	// Use this for initialization
	void Start () {
        offset = transform.position - targetObject.transform.position;
	}
	
	// Update is called once per frame
	void Update () {

        transform.position = targetObject.transform.position + offset;

	}
}
