﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoffeeTableScript : MonoBehaviour, IInteractable {

    public GameController gameControl; // Game Control object

    // Use this for initialization
    void Start () {
        gameControl = GameObject.Find("GameController").GetComponent<GameController>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Action()
    {
        if (gameControl.AllItemsCollected() == true)
        {
            gameControl.winMenu.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            gameControl.OpenDialogue("You still do not have all the items to make a coffee!");
        }

    }

    public void ResetInteractable()
    {
        return;
    }

}
