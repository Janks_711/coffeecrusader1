﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DarkImageScript : MonoBehaviour {

    public Image darkImage;

    // Use this for initialization
    void Start() {
        darkImage.gameObject.SetActive(false);
    }	

	// Update is called once per frame
	void Update () {
        if (SceneManager.GetSceneByName("Floor3Scene").isLoaded == true)
            darkImage.gameObject.SetActive(true);
        else
            darkImage.gameObject.SetActive(false);
    }

}
