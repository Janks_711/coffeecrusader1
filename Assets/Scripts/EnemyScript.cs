﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour {

    public GameObject[] waypoints;
    private GameObject currWaypoint;
    public float moveSpeed;
    private float distance;
    public Rigidbody2D enemyRigidBody;
    private Vector3 moveDir;
    int count;

    // Use this for initialization
    void Start () {
        count = 1;
        enemyRigidBody = GetComponent<Rigidbody2D>();
        transform.position = waypoints[0].transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        // get current waypoint
        currWaypoint = waypoints[count];

        // Set direction to current waypoint
        moveDir = currWaypoint.transform.position - transform.position;
        moveDir.Normalize();

        // Get distance to current waypoint
        distance = Vector3.Distance(currWaypoint.transform.position, transform.position);

        // Move towards current waypoint
        if (moveDir != Vector3.zero)
        {
            if (moveDir.x < 0)
                transform.rotation = Quaternion.Euler(0, 0, -180);
            else if (moveDir.x > 0)
                transform.rotation = Quaternion.Euler(0, 0, 0);

            if (moveDir.y > 0)
                transform.rotation = Quaternion.Euler(0, 0, 90);
            else if (moveDir.y < 0)
                transform.rotation = Quaternion.Euler(0, 0, -90);
        }

        // Once close enough to current waypoint, select next waypoint
        if (distance > 0.1f)
            enemyRigidBody.velocity = moveDir * moveSpeed * Time.deltaTime;
        else
        {
            if (count == 1)
            {
                count--;
                Debug.Log(count);
            }
            else
            {
                count++;
                Debug.Log(count);
            }
        }
    }
}
