﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    private float timeToCloseDialogue;

    public bool debugMode; // Whether the game should start in Debug mode
    public GameObject inGameMenu;
    public GameObject gameOverMenu;
    public GameObject winMenu;

    [Header("Timer")]
    public float timeLeft = 420.0f; //Timer for the length of time the player has to find the items
    public Text timerText; // UI text for the timer
    public bool timerStarted = false; // Should the timer have started

    [Header("Transition Fade")]
    public Image fadeImage; // Fade Image UI Component

    [Header("Dialogue")]
    public GameObject diaglougeBox; // Dialogue Box for disaply text to the user
    public Text dialogueTextInput;// Text to change

    // Check if items are collected
    [Header("Items")]
    public bool milkCollected;
    public bool coffeeCollected;
    public bool grinderCollected;
    public bool cupCollected;
    public bool spoonCollected;
    public bool sugarCollected;
    public bool waterCollected;
    private bool playerHasBeenTold = false;

    // Check if levels are done
    [Header("Level Control")]
    public bool level6Completed;
    public bool safeUnlocked;

    // When the Game Wakes
    private void Awake()
    {
        if(debugMode == false)
            SceneManager.LoadSceneAsync("GroundFloorScene", LoadSceneMode.Additive);

        Time.timeScale = 1;
    }

    // Use this for initialization
    void Start () {

        // Set everything to false
        // This is needed for when the game resets
        milkCollected = false;
        coffeeCollected = false;
        grinderCollected = false;
        cupCollected = false;
        spoonCollected = false;
        sugarCollected = false;
        waterCollected = false;
        playerHasBeenTold = false;
        level6Completed = false;
        safeUnlocked = false;

        if (fadeImage != null)
        {
            if (!fadeImage.gameObject.activeInHierarchy)
                fadeImage.gameObject.SetActive(true);
        }

        if (gameOverMenu != null)
        {
            gameOverMenu.SetActive(false);

        }

        if (winMenu != null)
        {
            winMenu.SetActive(false);

        }

        if (diaglougeBox != null)
        {
            if (diaglougeBox.gameObject.activeInHierarchy)
                diaglougeBox.gameObject.SetActive(false);
        }


    }
	
	// Update is called once per frame
	void Update () {
        Timer();

        // When game is over, open Game Over Menu
        if (timeLeft <= 0)
        {
            Time.timeScale = 0;

            if (gameOverMenu != null)
                gameOverMenu.SetActive(true);                
        }

        timeToCloseDialogue -= Time.deltaTime; // Timer in use so when the dialogue is opened there is a certain amount of time before dialogue can be closed 

        // Close the dialogue box
        if (Input.GetAxis("Interaction") != 0 && diaglougeBox.activeInHierarchy == true && timeToCloseDialogue <= 0)
        {
            CloseDialogue();
        }

        // Let the user know all items have been collected
        if (AllItemsCollected() == true && playerHasBeenTold == false && diaglougeBox.activeInHierarchy == false)
        {
            OpenDialogue("All Items Collected! Go Make Coffee, at the table on the ground floor!");
            playerHasBeenTold = true;
        }

        // Open the in game menu
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(inGameMenu.activeInHierarchy == false)
            {
                Time.timeScale = 0;
                inGameMenu.SetActive(true);
            }
            else if (inGameMenu.activeInHierarchy == true)
            {
                Time.timeScale = 1;
                inGameMenu.SetActive(false);
            }

        }

            
    }

    // Timer function
    void Timer()
    {
        // Using these so the Timer formaats in M:S
        string minutes;
        string seconds;
        string milliseconds;

        if (timerStarted == true)
            timeLeft -= Time.deltaTime;

        minutes = Mathf.Floor(timeLeft / 60.0f).ToString("00");
        seconds = Mathf.Floor(timeLeft % 60.0f).ToString("00");
        milliseconds = Mathf.Floor((timeLeft*100) %100).ToString("00");

        // If text object is applied display on screen
        if (timerText != null)
        {
            if (timeLeft > 60.0f)
                timerText.text = minutes + ":" + seconds;
            else if (timeLeft <= 60.0f && timeLeft >= 0.0f)
                timerText.text = seconds + "." + milliseconds;
            else
                timerText.text = "00:00";
        }
        else
            Debug.Log("No Timer Attached!");

    }

    // Opens the dialogue box and inputs Text
    public void OpenDialogue(string dialogueText)
    {
       
        if (diaglougeBox != null)
        {
            if (!diaglougeBox.gameObject.activeInHierarchy)
                diaglougeBox.gameObject.SetActive(true);
            dialogueTextInput.text = (dialogueText);
        }

        timeToCloseDialogue = 1.0f;
    }

    //Close Diaglogue Box
    public void CloseDialogue()
    {
        if (diaglougeBox != null)
        {
            if (diaglougeBox.gameObject.activeInHierarchy)
            {
                dialogueTextInput.text = ("");
                diaglougeBox.gameObject.SetActive(false);
            }
        }
    }

    // Check if all items are collected
    public bool AllItemsCollected()
    {
        if (milkCollected == true && coffeeCollected == true && grinderCollected == true && cupCollected == true && spoonCollected == true && sugarCollected == true && waterCollected == true)
            return true;
        else
            return false;
    }

}
