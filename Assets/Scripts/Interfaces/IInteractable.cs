﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractable {

    void Action(); // Perform an Action
    void ResetInteractable(); //Reset interactable Object
}
