﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroTransitionScript : MonoBehaviour {

    private bool transitioned = false; // Check if scene has transitioned

	// Use this for initialization
	void Start () {
        transitioned = false;
    }
	
	// Update is called once per frame
	void Update () {
		
        // Load main Scene
        if (Input.GetAxis("Interaction") != 0 && transitioned == false)
        {
            SceneManager.LoadScene("main");
            transitioned = true;
            
        }

	}
}
