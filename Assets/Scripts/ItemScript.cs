﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// Different Items
public enum ITEM
{
    MILK,
    CUP,
    COFFEE,
    SPOON,
    GRINDER,
    WATER,
    SUGAR

};

public class ItemScript : MonoBehaviour
{

    public Text itemText; // Item text for dialogue
    public GameController gameControl;
    public ITEM currentItem;

    private void Awake()
    {
        gameControl = GameObject.Find("GameController").GetComponent<GameController>();

        // Depending on which item is selected, displays different text. 
        switch (currentItem)
        {
            case ITEM.MILK:
                {
                    itemText = GameObject.Find("MilkText").GetComponent<Text>();

                    if (gameControl.milkCollected == true)
                        this.gameObject.SetActive(false);

                    break;
                }
            case ITEM.CUP:
                {
                    itemText = GameObject.Find("CupText").GetComponent<Text>();

                    if (gameControl.cupCollected == true)
                        this.gameObject.SetActive(false);

                    break;
                }
            case ITEM.COFFEE:
                {
                    itemText = GameObject.Find("CoffeeBeanText").GetComponent<Text>();

                    if (gameControl.coffeeCollected == true)
                        this.gameObject.SetActive(false);

                    break;
                }
            case ITEM.SPOON:
                {
                    itemText = GameObject.Find("SpoonText").GetComponent<Text>();

                    if (gameControl.spoonCollected == true)
                        this.gameObject.SetActive(false);

                    break;
                }
            case ITEM.GRINDER:
                {
                    itemText = GameObject.Find("GrinderText").GetComponent<Text>();

                    if (gameControl.grinderCollected == true)
                        this.gameObject.SetActive(false);

                    break;
                }
            case ITEM.WATER:
                {
                    itemText = GameObject.Find("WaterText").GetComponent<Text>();

                    if (gameControl.waterCollected == true)
                        this.gameObject.SetActive(false);

                    break;
                }
            case ITEM.SUGAR:
                {
                    itemText = GameObject.Find("SugarText").GetComponent<Text>();

                    if (gameControl.sugarCollected == true)
                        this.gameObject.SetActive(false);

                    break;
                }
        }
    }

    // Same as the above, depending on the item, do something for each item
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            switch (currentItem)
            {
                case ITEM.MILK:
                    {
                        gameControl.milkCollected = true;
                        itemText.color = Color.gray;

                        gameControl.diaglougeBox.SetActive(true);
                        gameControl.dialogueTextInput.text = "Milk Collected!";

                        break;
                    }
                case ITEM.CUP:
                    {
                        gameControl.cupCollected = true;
                        itemText.color = Color.gray;

                        gameControl.diaglougeBox.SetActive(true);
                        gameControl.dialogueTextInput.text = "Cup Collected!";

                        break;
                    }
                case ITEM.COFFEE:
                    {
                        gameControl.coffeeCollected = true;
                        itemText.color = Color.gray;

                        gameControl.diaglougeBox.SetActive(true);
                        gameControl.dialogueTextInput.text = "Coffee Collected!";

                        break;
                    }
                case ITEM.SPOON:
                    {
                        gameControl.spoonCollected = true;
                        itemText.color = Color.gray;

                        gameControl.diaglougeBox.SetActive(true);
                        gameControl.dialogueTextInput.text = "Spoon Collected!";

                        break;
                    }
                case ITEM.GRINDER:
                    {
                        gameControl.grinderCollected = true;
                        itemText.color = Color.gray;

                        gameControl.diaglougeBox.SetActive(true);
                        gameControl.dialogueTextInput.text = "Grinder Collected!";

                        break;
                    }
                case ITEM.WATER:
                    {
                        gameControl.waterCollected = true;
                        itemText.color = Color.gray;

                        gameControl.diaglougeBox.SetActive(true);
                        gameControl.dialogueTextInput.text = "'Fresh' Water Collected!";

                        break;
                    }
                case ITEM.SUGAR:
                    {
                        gameControl.sugarCollected = true;
                        itemText.color = Color.gray;

                        gameControl.diaglougeBox.SetActive(true);
                        gameControl.dialogueTextInput.text = "uhhhh close enough to sugar......";

                        break;
                    }
            }
            Destroy(this.gameObject);
        }
    }
}
