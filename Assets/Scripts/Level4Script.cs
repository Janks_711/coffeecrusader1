﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level4Script : MonoBehaviour {


    public bool allQuestionsAnswered = false; // Have all questions been answered
    public int questionsRemaining = 3; // How many questions remaining

    private GameController gameControl; // Game control
    public GameObject item; 
    public bool itemSpawned = false; // Check if item has already spawned

	// Use this for initialization
	void Start () {

        gameControl = GameObject.Find("GameController").GetComponent<GameController>();

        itemSpawned = false;

        if(gameControl.coffeeCollected != true)
        {
            questionsRemaining = 3;
            allQuestionsAnswered = false;
        }
        else
        {
            questionsRemaining = 0;
            allQuestionsAnswered = true;
        }
       
    }

    private void Update()
    {
        if (questionsRemaining == 0)
        {
            allQuestionsAnswered = true;
        }

        if (allQuestionsAnswered == true && itemSpawned == false)
        {
            item.SetActive(true);
            itemSpawned = true;
        }
    }

    // Function for when a question gets answered
    public void QuestionAnswered()
    {
        if (questionsRemaining > 0)
            questionsRemaining--;
    }
}
