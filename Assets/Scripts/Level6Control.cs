﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level6Control : MonoBehaviour {

    public GameObject item;
    public GameObject enemies;
    public GameController gameControl;
    public GameObject elevatorBlock;

    private int count;

	// Use this for initialization
	void Start () {
        gameControl = GameObject.Find("GameController").GetComponent<GameController>();
        enemies.SetActive(false);
        elevatorBlock.SetActive(false);
        count = 7;
	}
	
	// Update is called once per frame
	void Update () {
        if (enemies.GetComponentInChildren<EnemyScript>() == null)
        {
            if (item != null)
                if (item.activeInHierarchy == false && gameControl.sugarCollected == false)
                {
                    item.SetActive(true);
                    elevatorBlock.SetActive(false);
                    gameControl.level6Completed = true;
                }
        }

    }

    public void StartHallucinations()
    {
        enemies.SetActive(true);
        elevatorBlock.SetActive(true);
        gameControl.OpenDialogue("Kill the Hallucinations!\n Press 'F' to attack");
    }
}
