﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;


/// <summary>
/// This Scene will handle the transitioning between levels
/// </summary>


public class LevelTransition : MonoBehaviour {

    public string sceneToLoad; // Enter what scene you would like to load
    public GameController gameControl;
    public Animator fadeAnimation; // Fade animation
    public Image fadeImage; //Image that is fading
                                  
    // Use this for initialization
    void Start () {
        fadeImage = GameObject.Find("FadeImage").GetComponent<Image>(); // Find fade image
        fadeAnimation = GameObject.Find("FadeImage").GetComponent<Animator>(); // Find fade animation

        gameControl = GameObject.Find("GameController").GetComponent<GameController>();
    }
	
    // The trigger funtion is for the Elevator Transitions only
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            if (!SceneManager.GetSceneByName(sceneToLoad).isLoaded)
                SceneManager.LoadSceneAsync(sceneToLoad, LoadSceneMode.Additive);

            if (gameControl.timerStarted == false)
                gameControl.timerStarted = true;
        }

    }

    // When exiting the Trigger unload elevator Level
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            if (SceneManager.GetSceneByName(sceneToLoad).isLoaded)
                SceneManager.UnloadSceneAsync(sceneToLoad);
        }
    }

    // When Button is pressed in the Elevator run Coroutine
    public void ButtonLoadScene()
    {
        StartCoroutine(FadeTransition());
    }

    // Transition w/ Fade Funtion
    IEnumerator FadeTransition()
    {
        // For the way this works, with having 6 different levels and 2 scenes that the game should never transition 2
        // Need to determine what scenes are currently in the game
        // Then ONLY run the transition if the scene selected is not the main scene, the elevator scene or the current scene

        /// Dev Comment:: Really unsure why, but OR statements were not working..... So had to nest it like this
        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            if (SceneManager.GetSceneAt(i) != SceneManager.GetSceneByName("main"))
                if (SceneManager.GetSceneAt(i) != SceneManager.GetSceneByName("ElevatorScene")) 
                    if (SceneManager.GetSceneAt(i) != SceneManager.GetSceneByName(sceneToLoad))
                    {
                        if (fadeAnimation != null)
                            fadeAnimation.SetBool("bFade", true);
                        else
                            Debug.Log("Fade Animation Not set");

                        if(fadeImage != null)
                            yield return new WaitUntil(() => fadeImage.color.a == 1); // Waits until the screen is completely black until loading new level
                        else
                            Debug.Log("Fade Image Not set");

                        if (SceneManager.GetSceneAt(i).isLoaded)
                            SceneManager.UnloadSceneAsync(SceneManager.GetSceneAt(i));
                    }
        }

        // Load scene as specified in the strinfg box
        if (!SceneManager.GetSceneByName(sceneToLoad).isLoaded)
            SceneManager.LoadSceneAsync(sceneToLoad, LoadSceneMode.Additive);

        // Fade back into level
        fadeAnimation.SetBool("bFade", false);
    }

}
