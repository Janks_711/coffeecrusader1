﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButtonScript : MonoBehaviour {

    public GameObject inGamePanel;

    private void Start()
    {
        inGamePanel = GameObject.Find("InGameMenu");
    }

    // Start the game
    public void StartGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("main");
    }

    public void StartIntro()
    {
        SceneManager.LoadScene("IntroScene");
    }

    // Quit the application
    public void QuitGame()
    {
        Application.Quit();
    }

    // Resume the game
    public void ResumeGame()
    {
        Time.timeScale = 1;
        inGamePanel.SetActive(false);
    }

}
