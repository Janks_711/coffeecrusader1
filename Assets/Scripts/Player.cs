﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    public float movementSpeed; // Character Movement Speed
    private float currentMoveSpeed;

    [Range(0.0f, 1.0f)]
    public float diagonalMoveModifier; // Modify Diagonal Speeds

    public Rigidbody2D playerRigidBody; // Player Rigid Body

    public Text interactionText; // Interaction text

    public GameController gameControl; // game control Object

    private SpriteRenderer playerSprite;

    public GameObject whip;

    // Use this for initialization
    void Start () {
        playerRigidBody = this.GetComponent<Rigidbody2D>(); // Get Rigidbody Component on the player
        if (interactionText != null)
        {
            interactionText.text = "Press E";
            interactionText.gameObject.SetActive(false);
        }

        playerSprite = GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update () {

        Move(); // Move Character
    }

    // move function

    void Move()
    {
        if (Input.GetAxis("Horizontal") != 0)
            playerRigidBody.velocity = new Vector2(Input.GetAxisRaw("Horizontal") * currentMoveSpeed * Time.deltaTime, playerRigidBody.velocity.y);
        if (Input.GetAxis("Vertical") != 0)
           playerRigidBody.velocity = new Vector2(playerRigidBody.velocity.x, Input.GetAxisRaw("Vertical") * currentMoveSpeed * Time.deltaTime);

        //Stop player moving when button is released
        if (Input.GetAxisRaw("Horizontal") == 0)
        {
            playerRigidBody.velocity = new Vector2(0, playerRigidBody.velocity.y);
        }
        if (Input.GetAxisRaw("Vertical") == 0)
        {
            playerRigidBody.velocity = new Vector2(playerRigidBody.velocity.x, 0);
        }

        // This code restricts speed of the diagonal movement
        if (Mathf.Abs (Input.GetAxisRaw("Horizontal")) > 0.5f && Mathf.Abs (Input.GetAxisRaw("Vertical")) > 0.5f)
        {
            currentMoveSpeed = movementSpeed * diagonalMoveModifier;
        }
        else
        {
            currentMoveSpeed = movementSpeed;
        }

        Vector2 moveDir = playerRigidBody.velocity;

        if(moveDir != Vector2.zero)
        {
            if (moveDir.x < 0)
                transform.rotation = Quaternion.Euler(0, 0, -180);
            else if(moveDir.x > 0)
                transform.rotation = Quaternion.Euler(0, 0, 0);

            if (moveDir.y > 0)
                transform.rotation = Quaternion.Euler(0, 0, 90);
            if (moveDir.y < 0)
                transform.rotation = Quaternion.Euler(0, 0, -90);
        }

        Attack(); // Attack function. Attach function will check for fire

        //if (playerRigidBody.velocity.x < 0)
        //    playerSprite.flipX = true;
        //else if (playerRigidBody.velocity.x > 0)
        //    playerSprite.flipX = false;


    }

    public void OnTriggerStay2D(Collider2D other)
    {
        // Allows user to interact with interactable objects
        if (other.tag == "Interactable")
        {
            if (interactionText != null)
                interactionText.gameObject.SetActive(true);

            if (Input.GetAxis("Interaction") != 0)
                other.gameObject.GetComponent<IInteractable>().Action();
        }

        // this is how the player attacks the objects
        else if (other.tag == "Enemy")
        {
            if (Input.GetAxis("Fire1") != 0)
                Destroy(other.gameObject);

        }

    }

    // when you exit from the interactable object, reset the interactable
    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Interactable")
        {
            if (interactionText != null)
                interactionText.gameObject.SetActive(false);

            other.gameObject.GetComponent<IInteractable>().ResetInteractable();
        }
    }

    // Display the whip when attacking
    private void Attack()
    {
        if (whip != null)
        {
            if (Input.GetKeyDown(KeyCode.F))
                whip.SetActive(true);

            if (Input.GetKeyUp(KeyCode.F))
                whip.SetActive(false);
        }
            
    }
}
