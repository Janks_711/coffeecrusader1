﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuizButton : MonoBehaviour {

    public bool correctAnswer;

    public QuizMaster quizMaster;
    public GameController gameControl;

    public Level4Script level4Control;

	// Use this for initialization
	void Start () {
        gameControl = GameObject.Find("GameController").GetComponent<GameController>();
        level4Control = GameObject.Find("Level4Control").GetComponent<Level4Script>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // iF the quiz master hasn't answered a question, allows the user to make a choice
    // If right set the quizmaster to already answered
    // IF wrong player loses 30 seconds
    public void Action()
    {
        if(quizMaster.alreadyAnswered == false)
        {
            if (correctAnswer == false)
            {
                quizMaster.quizMasterText.text = "HA! That's your answer! Since you answer was so bad I am gonna waste some of your time! Feel free to try again though!";
                gameControl.timeLeft -= 30.0f;
                Debug.Log("WRONG");
            }

            if (correctAnswer == true)
            {
                level4Control.QuestionAnswered();

                switch (level4Control.questionsRemaining)
                {
                    case 2:
                        quizMaster.quizMasterText.text = "Lucky Guess! But you need to answer 2 more questions!";
                        break;
                    case 1:
                        quizMaster.quizMasterText.text = "Your right... But you won't get the last one so it doesn't matter";
                        break;
                    case 0:
                        quizMaster.quizMasterText.text = "Wow..... You must have cheated! Fine here is your one Bean! Now leave me to work!";
                        break;

                }

                quizMaster.alreadyAnswered = true;
            }
        } 
    }

    // Set the quiz master Object
    public void SetQuizMaster(QuizMaster other)
    {
        quizMaster = other;
    }

}
