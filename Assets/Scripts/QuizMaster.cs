﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuizMaster : MonoBehaviour, IInteractable {

    public GameObject quizInterface; // The interface for the Quiz Questions
    public GameController gameControl; // game control Object
    public bool alreadyAnswered; // Check whether this quizmaster has already answered his question
    public Text quizMasterText; // The text for the quiz master

    public string introText;
    
    // Use this for initialization
    void Start () {
        gameControl = GameObject.Find("GameController").GetComponent<GameController>();

        introText = quizMasterText.text;
	}

    public void Action()
    {
        if(alreadyAnswered == false && gameControl.coffeeCollected == false)
        {
            quizInterface.SetActive(true);
            
        }
        else if (alreadyAnswered == true || gameControl.coffeeCollected == true)
        {
            gameControl.OpenDialogue("You already have my coffee bean! Now I need to work! Go bother someone else");
        }
    }

    public void ResetInteractable()
    {
        quizInterface.SetActive(false);
        quizMasterText.text = introText;
    }

}
