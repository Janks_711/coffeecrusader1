﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SafeScript : MonoBehaviour, IInteractable {

    public GameObject safeUIPanel; // Safecanvas object

    public int[] safecode = { 5, 7, 8, 9, 6, 2 }; // The code the user needs to input
    public int[] userInputNumber; // The code currently input

    //Animations and animation Images
    public Animator[] numberBlink;
    public Image[] blinkImage;
    private Color color; // Color variable for the color of the blink Image

    public Text[] inputText; // The Number text that will appear on the safe screen

    private Text currText; // Current Text selected
    private int currEntry; //current number

    private bool safeUnlocked; // Determine if the safe is unlocked
    private bool safeOpened; //This will check if safe open as already been run, once it has been run once it shouldn't run again

    public GameObject item; // Get the item in the current scene
    public GameController gameControl; // Get the game control Objcet

    public Text lockedText; // Text to indicate the safe is locked

    // Use this for initialization
    void Start () {

        gameControl = GameObject.Find("GameController").GetComponent<GameController>();

        // Maks sure the Canvas is closed on startup
        if (safeUIPanel != null)
            safeUIPanel.SetActive(false);

        // Make sure item is also turned off
        if (item != null)
            item.SetActive(false);

        // Safe will always start as locked
        safeUnlocked = false;
        safeOpened = false;

        // Set text to locked and color red
        if (lockedText != null)
        {
            lockedText.text = "Locked";
            lockedText.color = Color.red;
        }
        

        // Create a color the same as the image and set the alpha as 0
        // This stops the blink image from being stuck on the safe
        color = blinkImage[0].color;
        color.a = 0;

        // Disable all blink Animations
        foreach (Animator i in numberBlink)
        {
            i.enabled = false;
        }

        // Set the current entry to the first number
        currEntry = 0;
        currText = inputText[currEntry];
        numberBlink[currEntry].enabled = true;
	}
	
	// Update is called once per frame
	void Update () {

        // If an entry is made, go to the very next entry
        if (currText.text != "" && currEntry < 5)
        {
            numberBlink[currEntry].enabled = false;
            blinkImage[currEntry].color = color;
            currEntry++;
            currText = inputText[currEntry];
            numberBlink[currEntry].enabled = true;
        }

        // Check if the codes match
        // If they do unlock the safe
        CheckIfCodesMatch(); 

        if (safeUnlocked == true && safeOpened == false)
        {
            SafeOpen();
            Debug.Log("Unlocked");
        }
	}

    // Interactable Action to Perform, in this case open the UI
    public void Action()
    {
        if (gameControl.safeUnlocked == false)
        {
            if (safeUIPanel != null)
            {
                if(safeUIPanel.activeInHierarchy == false)
                {
                    safeUIPanel.SetActive(true);
                }
            }
                
        }
        else
            gameControl.OpenDialogue("Safe is already Unlocked");

    }

    // What to do when reset the interactable
    // In this case, close safeUI and reset the safe
    public void ResetInteractable()
    {
        if (safeUIPanel != null)
            if(safeUIPanel.activeInHierarchy == true)
                safeUIPanel.SetActive(false);

        if (gameControl.safeUnlocked == false)
            ResetSafe();
    }

    // The number click button to assign to the buttons on the safe
    public void NumberClicked(int numberClicked)
    {
        if (safeUnlocked == false)
        {
            string tempString = numberClicked.ToString();
            userInputNumber[currEntry] = numberClicked;
            currText.text = tempString;
        }

    }

    // Reset the safe to empty
    public void ResetSafe()
    {
        if (safeUnlocked == false)
        {
            currEntry = 0;

            foreach (Text i in inputText)
            {
                i.text = "";
            }

            for (int i = 0; i < 6; i++)
            {
                userInputNumber[i] = 0;
            }


            foreach (Animator i in numberBlink)
            {
                if (i.enabled == true)
                    i.enabled = false;
            }

            currText = inputText[currEntry];
            numberBlink[currEntry].enabled = true;
        }


    }

    // Check if the code matches what is inputted by the user
    private void CheckIfCodesMatch()
    {
        for(int i = 0; i < 6; i++)
        {
            if (safecode[i] != userInputNumber[i])
            {
                safeUnlocked = false;
                break;
            }

            else
                safeUnlocked = true;

        }
    }

    // Function for what happens when the safe is open
    public void SafeOpen()
    {
        if (gameControl.safeUnlocked == false)
        {
            item.SetActive(true);
            gameControl.safeUnlocked = true;
            
            if(lockedText != null)
            {
                lockedText.text = "Unlocked";
                lockedText.color = Color.green;
            }

            safeOpened = true;
        }
    }
}
