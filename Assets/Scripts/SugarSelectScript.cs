﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SugarSelectScript : MonoBehaviour, IInteractable {

    public GameController gameControl;
    public Level6Control level6Control;

    // Use this for initialization
    void Start () {
        gameControl = GameObject.Find("GameController").GetComponent<GameController>();

        if (gameControl.level6Completed == true)
            Destroy(this.gameObject);

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    // Only allow this action id Sugar has not already been collected
    public void Action()
    {
        if(gameControl.level6Completed == false)
        {
            level6Control.StartHallucinations();
        }
        Destroy(this.gameObject);
    }

    public void ResetInteractable()
    {
        return;
    }

    // Display dialogue when user approaches the sugar
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "HitBox" && gameControl.level6Completed == false)
        {
            gameControl.OpenDialogue("Ummm, it looks like sugar? Why would the boss have sugar on his desk? I guess I should try it!\n Press 'E' to try the Sugar");
        }
    }
}