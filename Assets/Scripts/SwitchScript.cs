﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchScript : MonoBehaviour, IInteractable {

    /// <summary>
    /// Switch Script that performs an action on a Target object
    /// At this stage will only make an object disspear
    /// </summary>

    public GameObject targetObject;
    private bool hasBeenSwtiched = false;

    // Check if switch has been switched, if it hasn't switch it on
    // if it has switch it off
    public void Action()
    {
        if (targetObject != null)
            if (targetObject.activeInHierarchy == true && hasBeenSwtiched == false)
            {
                targetObject.SetActive(false);
                hasBeenSwtiched = true;
            }
            else if (targetObject.activeInHierarchy == false && hasBeenSwtiched == false)
            {
                targetObject.SetActive(true);
                hasBeenSwtiched = true;
            }
                

    }

    public void ResetInteractable()
    {
        hasBeenSwtiched = false;
    }
}
